<?php

namespace App\Http\Controllers;

use App\Category;

class ProductsController extends Controller {

	public function index() {
		$posts = Products::with('category')->latest()->paginate(15);

		return view('products.index', compact('category'));
	}

	public function create() {
		$categories = Category::orderBy('name')->get();
		return view('products.create', compact('categories'));
	}

}

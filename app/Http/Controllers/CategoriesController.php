<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller {

	public function index() {

		$categories = Category::get();

		return view('admin.categories.index', compact('categories'));
	}

	public function create() {
		$levels = Category::where(['parent_id' => 0])->get();
		return view('admin.categories.create')->with(compact('levels'));
	}

	public function store(Request $request) {
		$category = new Category;
		$category->name = $request->category_name;
		$category->description = $request->description;
		$category->parent_id = $request->parent_id;
		$category->url = $request->url;
		$category->save();
		return redirect('admin/categories')->with('flash_message_success', 'Categoria inserita con successo.');

	}

	public function edit(Category $category) {
		$levels = Category::where(['parent_id' => 0])->get();
		return view('admin.categories.edit', compact('category', 'levels'));
	}

	public function update(Request $request, Category $category) {
		$category->name = $request->category_name;
		$category->description = $request->description;
		$category->parent_id = $request->parent_id;
		$category->url = $request->url;
		$category->update();
		//return redirect()->route('categories.show', $category);
		return redirect()->route('categories.index')->with('flash_message_success', 'Categoria modificata con successo.');
	}

	public function show(Category $category) {
		return view('admin.categories.show', compact('category'));
	}

	public function destroy(Category $category) {
		$category->delete();
		return redirect('admin/categories')->with('flash_message_success', 'Categoria rimossa con successo.');
	}
}

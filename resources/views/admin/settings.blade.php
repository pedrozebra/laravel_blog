@extends('layouts.adminLayout.admin_design')

@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Opzioni</a> </div>
    <h1>Opzioni Admin</h1>
  </div>
  <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Aggiorna Password</h5>
            </div>
            <div class="widget-content nopadding">
				      @if(Session::has('flash_message_error'))
                	<div class="alert alert-error alert-block">
                    	<button type="button" class="close" data-dismiss="alert">x</button>
                    	<span>{!! session('flash_message_error') !!}</span>
                	</div>
            	@endif
	            @if(Session::has('flash_message_success'))
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <span>{!! session('flash_message_success') !!}</span>
	                </div>
	            @endif
              <form class="form-horizontal" method="post" action="{{ url('/admin/update-pwd') }}" name="password_validate" id="password_validate" novalidate="novalidate">
              	@csrf
                <div class="control-group">
                  <label class="control-label">Password corrente</label>
                  <div class="controls">
                    <input type="password" name="current_pwd" id="current_pwd" />
                    <span id="chkPwd"></span>
                  </div>
                </div>
                 <div class="control-group">
                  <label class="control-label">Nuova password</label>
                  <div class="controls">
                    <input type="password" name="new_pwd" id="new_pwd" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Conferma password</label>
                  <div class="controls">
                    <input type="password" name="confirm_pwd" id="confirm_pwd" />
                  </div>
                </div>
                <div class="form-actions">
                  <input type="submit" value="Aggiorna Password" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

  </div>
</div>

@endsection
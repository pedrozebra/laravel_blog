@extends('layouts.adminLayout.admin_design')
@section('content')


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ route('categories.index') }}" class="current">Categorie</a> </div>
    <h1>Categorie</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
        	 @if(Session::has('flash_message_error'))
                	<div class="alert alert-error alert-block">
                    	<button type="button" class="close" data-dismiss="alert">x</button>
                    	<span>{!! session('flash_message_error') !!}</span>
                	</div>
            	@endif
	            @if(Session::has('flash_message_success'))
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <span>{!! session('flash_message_success') !!}</span>
	                </div>
	            @endif
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Elenco Categorie</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nome</th>
                  <th>URL</th>
                  <th>Azioni</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($categories as $category)
                <tr class="gradeX">
                  <td>{{ $category->id }}</td>
                  <td>{{ $category->name }}</td>
                  <td>{{ $category->url }}</td>
                  <td class="center">
                  	<a href="{{ route('categories.edit', $category) }}" class="btn btn-primary btn-mini">Modifica</a>
                  	<a href="{{ route('categories.show', $category) }}" class="btn btn-success btn-mini">Dettaglio</a>
                  	<form action="{{ route('categories.destroy', $category) }}" method="post">
                  		@csrf
                  		@method('DELETE')
                  		<button class="btn btn-danger btn-mini" id="destroy_cat">Elimina</button>
                  	</form>

                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection
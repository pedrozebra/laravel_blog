@extends('layouts.adminLayout.admin_design')
@section('content')


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ route('categories.index') }}" class="current">Categorie</a> </div>
    <h1>Dettaglio Categoria</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
        	 @if(Session::has('flash_message_error'))
                	<div class="alert alert-error alert-block">
                    	<button type="button" class="close" data-dismiss="alert">x</button>
                    	<span>{!! session('flash_message_error') !!}</span>
                	</div>
            	@endif
	            @if(Session::has('flash_message_success'))
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <span>{!! session('flash_message_success') !!}</span>
	                </div>
	            @endif
          <div class="widget-content nopadding">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nome</th>
                  <th>Descrizione</th>
                  <th>URL</th>
                </tr>
              </thead>
              <tbody>

                <tr class="gradeX">
                  <td>{{ $category->id }}</td>
                  <td>{{ $category->name }}</td>
                  <td>{{ $category->description }}</td>
                  <td>{{ $category->url }}</td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection
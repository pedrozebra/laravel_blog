@extends('layouts.adminLayout.admin_design')
@section('content')


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ route('categories.index') }}" class="current">Categorie</a> </div>
    <h1>Modifica Categoria</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
        	 @if(Session::has('flash_message_error'))
                	<div class="alert alert-error alert-block">
                    	<button type="button" class="close" data-dismiss="alert">x</button>
                    	<span>{!! session('flash_message_error') !!}</span>
                	</div>
            	@endif
	            @if(Session::has('flash_message_success'))
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <span>{!! session('flash_message_success') !!}</span>
	                </div>
	            @endif
           <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ route('categories.update', $category) }}" name="edit_category" id="edit_category" novalidate="novalidate">
              @csrf
              @method('PATCH')
              <div class="control-group">
                <label class="control-label">Nome Categoria</label>
                <div class="controls">
                  <input type="text" name="category_name" id="category_name" value="{{ old('name', $category->name) }}">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">Livello</label>
                <div class="controls">
                  <select name="parent_id" id="parent_id" style="width:200px">
                    <option value="0">Categoria Principale</option>
                    @foreach($levels as $level)
                     <option value="{{ $level->id }}"
                        @if($level->id == old('id', $level->id))
                            selected="selected"
                        @endif
                        >{{ $level->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

             <div class="control-group">
              <label class="control-label">Descrizione</label>
                <div class="controls">
                  <textarea name="description" id="description">{{ old('description', $category->description) }}</textarea>
                </div>
             </div>
            <div class="control-group">
              <label class="control-label">URL</label>
                <div class="controls">
                  <input type="text" name="url" id="url" value="{{ old('url', $category->url) }}">
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Modifica" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection